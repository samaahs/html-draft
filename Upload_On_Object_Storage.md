<img src="images/IDSNlogo.png" width="200" height="200"/>
<br> <br> <br> 

# Hands-on lab on Hosting your application IBM Cloud Object Storage 

## Overview

Once you have developed your web application, you need to host the files on web server so others on the internet can access from their web browser. But rather than setup your own webserver, you can host your files on the Cloud. In this lab you will upload your files to IBM Cloud Object Storage - which provides an option to host static web sites.

#### Duration (25 mins)

### Objective

After completing this lab you will be able to:

1. Host your application using Object storage

## Prerequisites:

You will need an IBM Cloud account to do this lab. If you have not created one already, click on this  <a href="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/CreateIBMCloudAccount.md.html?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDeveloperSkillsNetworkCC0101ENSkillsNetwork19843267-2021-01-01">link </a> and follow the instructions to create an IBM Cloud account.

## Task 1 - Create an instance of IBM Cloud Object Storage: 

1.  Log in to your IBM Cloud account and open the IBM Cloud Catalog [https://cloud.ibm.com/catalog](https://cloud.ibm.com/catalog?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDeveloperSkillsNetworkCC0101ENSkillsNetwork19843267-2021-01-01).

2.  On the Catalog page, search and select **Object Storage**:

<img src="images/UOOS_Task_1.2.png">

3.  On the Cloud Object Storage (COS) page, choose the Lite plan. You can use the name that is shown in Service name or rename it as you want, accept the Default resource group, and then click Create.

<img src="images/UOOS_Task_1.3.png">

Once the Cloud Object Storage (COS) instance is created, you will automatically be directed to the Cloud Object Storage page. Here you can create a bucket and invite users for your Cloud Object Storage instance.

## Task 2 - Creating and Adding items to your Bucket

1. On your new Object Storage instance page, select on **Bucket** from the panel on the left and click on **Create Bucket**

<img src="images/UOOS_Task_2.1.png">

2. On the Create a bucket page, we will crete a create a predefined bucket, under which select the *Host a static website*. 

<img src="images/UOOS_Task_2.2.png">

3. Use the pre-assigned name for your bucket or enter your name and review. Scroll down and enable the public access so that your calculator will be visible to outsiders. Next, scroll further down and click *next*.

<img src="images/UOOS_Task_2.3.png">

4. The Bucket page appears where you can begin adding objects to the bucket. Click on the *upload* option and select the 3 files of your calculator you downloaded in the previous lab (index.html, script.js & style.css).

Once you have uploaded the files successfully, you should be seeing a success message along with the number of files being uploaded in the bucket. Close and click next

> **NOTE:** You might not be able to see the file names after uploading on the upload screen.

<img src="images/UOOS_Task_2.4.png">

5. Now on the *Test your bucket* page, scroll all the way down and click on **View bucket configuration**

<img src="images/UOOS_Task_2.5.png">

6. You will be redirected to the bucket configuration.

## Task 3 - Test your bucket

1. Click on the **Objects** tab and take a look at the files that were uploaded in the previous task.

<img src="images/UOOS_Task_3.1.png">

> **NOTE:** Make sure the name of the files are correct and does not start with *undefined* etc. If incase of a different file uploaded, delete the file by clicking on ellipses and click delete. Then Upload a the correct file.

2. Go back to **configuration** tab. You will see all of the bucket related details in this tab.

3. Now Scroll down on the **configuration** tab. Under the **Endpoint** Section, Copy the public URL. (You will be asked to submit this URL for Peer-review)

<img src="images/UOOS_Task_3.3.png">

4. Let's check if your app is running fine on the public URL or not. Open an incognito window/a different browser/tab. Paste the URL copied previously and paste it. Run some tests to see if your application is running fine.

<img src="images/UOOS_Task_3.4.png">

Congratulations! You have successfully completed this lab and your assignment!

## Author(s)
<a href="https://www.linkedin.com/in/ssamaah/">Samaah</a>


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 25-Feb-2022 | 1.0 | Samaah | Initial version created |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2022. All rights reserved. <h3/>

