<img src="images/IDSNlogo.png" width="200" height="200"/>
<br> <br> <br> 

#### Duration (25 mins)

# Hands-on lab on Javascript 

### Objective

After completing this lab you will be able to:

1. Convert Numbers to Strings.
2. Convert Strings to Numbers.


## Task 1 - Open the browser console. 

In this task, we are going to run the Javascript code in the browser console. The Chrome browser has a v8, which is Google's open source high-performance JavaScript engine.

1. Open a new blank browser page clicking on Ctrl+T(Windows) or Command+T(Mac) to open a new tab.

2. Right-click anywhere on the new blank browser tab and choose **Inspect** or **Inspect Element** depending on the browser you are using. The image below is for Chrome Broswer.

<img src="images/inspect_element.png" style="width : 50%;margin-left:30"/>

3. Go to the **Console** tab, as shown below. You will see a command prompt. You can run the rest of the tasks there. 

<img src="images/browser_console.png" style="width : 50%;margin-left:50"/>

4. If your console has any logs printed, clear it by running the following command. This is not mandatory. It will just help in a fresh start.

```js
clear()
```
{: codeblock}

## Task 2 - Clearing the console

> **NOTE:** At any point of time, when you want to clear the console run the `clear()` command.Use `console.clear()` if `clear()` doesnt works.

To run the commands we will use the command prompt on the browser control. Type or paste the command and press **enter** to run the command.

# Exercise 1 - Converting Numbers to Strings

## EX 1.1 

1. toString() is a method that can be used to convert a number data type to a string data type. It returns the value of the number as a string.

```js
Syntax: 
    number.toString(radix)
```
{: codeblock}

> **Note:** *radix* is an optional parameter. It is used to define the base in which the conversion can be done in the form of a string.

2. Let's take a small exercise to understand it better.

>Both let and var can be used to create variables. `var` is used when you want the variable to have global scope and `let` is used when you want the variable to have scope within the block where it is created.

```js
//Checking the data type of a variable
let num = 5;
console.log(typeof num);
```


The output will be as below,
```html
number
```

> **NOTE:** The **typeof** keyword in JavaScript is used to find out the data type of the supplied operand.

To convert variable num from a number to string type we use the **toString()** Method

```js
//Converting number to a string without radix
let num = 5;
console.log("Previous data type of num was: "+ typeof num);
let str = num.toString();
console.log("Current data type of num is: " + typeof str);
```


The output will be as below,

```html
Previous data type of num was: number
Current data type of num is: string
```

## EX 1.2

1. ***Radix*** is defined according to the base to be used. It can have Base 2 (Binary), Base 8 (Octal), Base16 (Hexadecimal). It is an optional parameter.

2. In this example, Notice how a number can be converted according to the radix and still can be converted as a string. Where initially num1 = 10 and logging the type shows number. Using radix 2 converts 10 into a binary digit i.e. 1010 and is yet converted to string. Same goes with radix 8 and 16 respectively

```js
//Converting number to a string with radix
let num1 = 10;
console.log(typeof num1);

//Binary
let str1 = num1.toString(2);
console.log("With binary base 2: " + str1);
console.log(typeof str1);

//Octal
str1 = num1.toString(8);
console.log("With Octal base 8: " + str1);
console.log(typeof str1);

//Hexadecimal
str1 = num1.toString(16);
console.log("With Hexadecimal base 16: " + str1);
console.log(typeof str1);
```

The output will be as below,

```html
number
With binary base 2: 1010
string
With octal base 8: 12
string
With Hexadecimal base 16: a
string
```

## EX 1.3 - Test your understanding

### Solve this:

1. Convert a number 4856 to a string. Don’t forget to check the type of the variable before and after conversion.

<details><summary> Click to see the code </summary>

```js
//Converting number to a string with radix
let num = 4856;
console.log("Previous data type was: "+ typeof num);
let str = num.toString();
console.log("Current data type is: "+ typeof str);
```

</details>
The output will be as below,

```html
Previous data type was: number
Current data type is: string
```

2. Convert 14 number to a string using base 2, 8 and 10.

<details><summary> Click to see the code </summary>

```js
let num = 14;
console.log(typeof num);

//Binary
let str1 = num.toString(2);
console.log("With binary base 2: " + str1);
console.log(typeof str1);

//Octal
str1 = num.toString(8);
console.log("With Octal base 8: " + str1);
console.log(typeof str1);

//Hexadecimal
str1 = num.toString(16);
console.log("With Hexadecimal base 16: " + str1);
console.log(typeof str1);

```

</details>

The output will be as below,

```html
number
With binary base 2: 1110
string
With octal base 8: 16
string
With Hexadecimal base 16: e
string
```

# Exercise 2 - Converting Strings to Numbers

***Number()*** method is used to convert a string to a number type.

## EX 2.1 

1. Lets take a small exercise to understand it better.

```js
//Checking the data type of a variable
let str = "5 "
console.log("Previous data type was: "+ typeof str);
let num = Number(str)
console.log("Current data type is: "+ typeof num);
```

The output will be as below,

```html
Previous data type was: string
Current data type is: number
```

## EX 2.2 

1. There are 2 more methods that can be used to convert a string to an integer type and float type number specifically:

    1. ***parseInt()*** It returns a whole number. In case of alphabets, it returns NaN (Not a Number).
    2. ***parseFloat()*** It returns a number with a decimal if mentioned. In case of alphabets, it returns NaN (Not a Number).

Let's try them out

```js
let str = "5.5";
let str1= "hello 10";
console.log("Previous data type for str was: "+ typeof str + " With the value of: " + str);
console.log("Previous data type for str1 was: "+ typeof str1+ " With the value of: " + str1);

//Now lets try converting it in integer value using parseInt()
var i1= parseInt(str);
var i2= parseInt(str1);
console.log("using parseInt() data type of i1 is: "+ typeof i1 + " And the value is: " + i1);
console.log("using parseInt() data type of i2 is: "+ typeof i2 + " And the value is: " + i2);

//Now lets try converting it in integer value using parseFloat()
var f1= parseFloat (str);
var f2= parseFloat(str1);
console.log("using parseFloat() data type of f1 is "+ typeof f1 + " And the value is: " + f1);
console.log("using parseFloat() data type of f2 is "+ typeof f2  + " And the value is: " + f2);
```

The output will be as below,

```html
Previous data type for str was: string With the value of: 5.5
Previous data type for str1 was: string With the value of: hello 10
using parseInt() data type of i1 is: number And the value is: 5
using parseInt() data type of i2 is: number And the value is: NaN
using parseFloat() data type of f1 is number And the value is: 5.5
using parseFloat() data type of f2 is number And the value is: NaN 
```

## EX 2.3 - Test your understanding

### Solve this:

1. Convert the strings “hello world” “28.9” to Number(), parseInt, and parseFloat()

<details><summary> Click to see the code </summary>

```js
//Converting number to a string with radix
let str1 = "hello world";
let str2= "28.9 ";
console.log("Previous data type for str1 was: "+ typeof str1 + " with the value of: " + str1);
console.log("Previous data type for str2 was: "+ typeof str1+ " with the value of: " + str2);

var n1 = Number(str1);
var n2 = Number(str2);
console.log("using parseInt() data type of n1 is: "+ typeof n1 + " And the value is: " + n1);
console.log("using parseInt() data type of n2 is: "+ typeof n2 + " And the value is: " + n2);

var i1= parseInt(str1);
var i2= parseInt(str2);
console.log("using parseInt() data type of i1 is: "+ typeof i1 + " And the value is: " + i1);
console.log("using parseInt() data type of i2 is: "+ typeof i2 + " And the value is: " + i2);

var f1= parseFloat (str1);
var f2= parseFloat(str2);
console.log("using parseFloat() data type of f1 is "+ typeof f1 + " And the value is: " + f1);
console.log("using parseFloat() data type of f2 is "+ typeof f2  + " And the value is: " + f2);
```
</details>
The output will be as below,

```HTML
Previous data type for str1 was: string with the value of: hello world
Previous data type for str2 was: string with the value of: 28.9 
using parseInt() data type of n1 is: number And the value is: NaN
using parseInt() data type of n2 is: number And the value is: 28.9
using parseInt() data type of i1 is: number And the value is: NaN
using parseInt() data type of i2 is: number And the value is: 28
using parseFloat() data type of f1 is number And the value is: NaN
using parseFloat() data type of f2 is number And the value is: 28.9
```

## Author(s)
<a href="https://www.linkedin.com/in/ssamaah/">Samaah</a>


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 17-Feb-2022 | 1.0 | Samaah | Initial version created |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2022. All rights reserved. <h3/>

